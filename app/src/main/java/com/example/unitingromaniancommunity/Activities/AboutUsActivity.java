package com.example.unitingromaniancommunity.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.unitingromaniancommunity.R;

public class AboutUsActivity extends AppCompatActivity {
    TextView about_us;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        about_us = findViewById(R.id.about_us);

        about_us.setText(R.string.dumy_text + R.string.dumy_text);
    }
}
