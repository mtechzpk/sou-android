package com.example.unitingromaniancommunity.Activities.PostActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.androidbuts.multispinnerfilter.SpinnerListener;
import com.example.unitingromaniancommunity.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class PostEventActivity extends AppCompatActivity {

    private EditText event_name, event_address, event_city, event_zip_code, phone_number, event_website,
            event_day, event_hours, admission_price, event_desc, confirm_password;
    private Spinner sp_state, sp_language_spoken;
    private MultiSpinnerSearch sp_event_cat;
    private View event_img;
    private ImageView back_btn;
    private Button btn_post_event;
    private String e_name = "", e_address = "", e_city = "", e_zip_code = "", e_ph_number = "", e_web = "",
            e_day = "", e_hours = "", e_price = "", e_desc = "", c_password = "", state = "", language = "", event_cat = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_event);
        InitViews();
        AdaptersData();
        ClickViews();
    }

    private void InitViews() {
        event_name = findViewById(R.id.event_name);
        event_address = findViewById(R.id.event_address);
        event_city = findViewById(R.id.city);
        event_zip_code = findViewById(R.id.event_zip_code);
        phone_number = findViewById(R.id.phone_number);
        event_website = findViewById(R.id.event_website);
        event_day = findViewById(R.id.event_day);
        event_hours = findViewById(R.id.event_hours);
        admission_price = findViewById(R.id.admission_price);
        event_desc = findViewById(R.id.event_desc);
        confirm_password = findViewById(R.id.confirm_password);

        sp_state = findViewById(R.id.sp_state);
        sp_language_spoken = findViewById(R.id.sp_language_spoken);
        sp_event_cat = findViewById(R.id.sp_event_cat);

        event_img = findViewById(R.id.event_img);

        back_btn = findViewById(R.id.back_btn);

        btn_post_event = findViewById(R.id.btn_post_event);
    }

    private void ClickViews() {
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_post_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                e_name = event_name.getText().toString().trim();
                e_address = event_address.getText().toString().trim();
                e_city = event_city.getText().toString().trim();
                e_zip_code = event_zip_code.getText().toString().trim();
                e_ph_number = phone_number.getText().toString().trim();
                e_web = event_website.getText().toString().trim();
                e_day = event_day.getText().toString().trim();
                e_hours = event_hours.getText().toString().trim();
                e_price = admission_price.getText().toString().trim();
                e_desc = event_desc.getText().toString().trim();
                c_password = confirm_password.getText().toString().trim();

            }
        });
    }

    private void AdaptersData() {

        ArrayAdapter<CharSequence> adapter_state = ArrayAdapter.createFromResource(this,
                R.array.state_array, android.R.layout.simple_spinner_item);
        adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_state.setAdapter(adapter_state);
        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                state = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"state",state);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_lang = ArrayAdapter.createFromResource(this,
                R.array.language_array, android.R.layout.simple_spinner_item);
        adapter_lang.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_language_spoken.setAdapter(adapter_lang);
        sp_language_spoken.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                language = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"language",language);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayList<String> event_cat_list = new ArrayList<>();
        event_cat_list.add("E Cat 1");
        event_cat_list.add("E Cat 2");
        event_cat_list.add("E Cat 3");
        event_cat_list.add("E Cat 4");
        event_cat_list.add("E Cat 5");
        event_cat_list.add("E Cat 6");

        final List<KeyPairBoolData> sportsArray0 = new ArrayList<>();
        for (int i = 0; i < event_cat_list.size(); i++) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setId(i + 1);
            h.setName(event_cat_list.get(i));
            h.setSelected(false);
            sportsArray0.add(h);
        }
        sp_event_cat.setItems(sportsArray0, -1, new SpinnerListener() {

            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
                ArrayList<String> getSports = new ArrayList<>();
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).isSelected()) {
                        Toast.makeText(PostEventActivity.this, items.get(i).getName() + " : " + items.get(i).isSelected(), Toast.LENGTH_SHORT).show();
                        getSports.add(items.get(i).getName());
                    }
                }
            }
        });

    }
}
