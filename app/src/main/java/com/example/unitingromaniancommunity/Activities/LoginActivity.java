package com.example.unitingromaniancommunity.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.unitingromaniancommunity.Activities.Extras.Utilities;
import com.example.unitingromaniancommunity.Activities.LoginDetails.CreateBusinessProfileActivity;
import com.example.unitingromaniancommunity.Activities.LoginDetails.CreateProfileActivity;
import com.example.unitingromaniancommunity.Activities.LoginDetails.ForgotPasswordActivity;
import com.example.unitingromaniancommunity.R;

public class LoginActivity extends AppCompatActivity {

    private EditText user_email, user_password;
    private RadioButton rb_remember_me;
    private TextView text_forgot_pass;
    private Button btn_sign_in, btn_sign_up;
    private String email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        InitViews();
        ClickViews();
    }

    private void InitViews() {
        user_email = findViewById(R.id.user_email);
        user_password = findViewById(R.id.user_password);

        rb_remember_me = findViewById(R.id.rb_remember_me);

        text_forgot_pass = findViewById(R.id.text_forgot_pass);

        btn_sign_in = findViewById(R.id.btn_sign_in);
        btn_sign_up = findViewById(R.id.btn_sign_up);
    }

    private void ClickViews() {
        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = user_email.getText().toString().trim();
                password = user_password.getText().toString().trim();
                if (rb_remember_me.isChecked()){
                    Utilities.saveString(LoginActivity.this, "pass", password);
                }
                if(!email.isEmpty()){
                    if (!password.isEmpty()){
                        Toast.makeText(LoginActivity.this, email+" "+password, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, CreateBusinessProfileActivity.class);
                        startActivity(intent);
                    }else {
                        user_password.setError("Password Required");
                    }
                }else {
                    user_email.setError("Email Required");
                }
            }
        });

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, CreateProfileActivity.class);
                startActivity(intent);
            }
        });

        text_forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }
}
