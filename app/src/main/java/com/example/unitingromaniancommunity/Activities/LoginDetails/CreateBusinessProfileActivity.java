package com.example.unitingromaniancommunity.Activities.LoginDetails;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinner;
import com.androidbuts.multispinnerfilter.MultiSpinnerListener;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.androidbuts.multispinnerfilter.SpinnerListener;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.PriorityQueue;

public class CreateBusinessProfileActivity extends AppCompatActivity {

    private EditText business_name, business_address, city, zip_code_optional,
            business_phone, business_website, business_desc, confirm_password;
    private Spinner sp_state, sp_business_hours, sp_language_spoken;
    private MultiSpinnerSearch sp_business_cat;
    private View profile_img;
    private ImageView back_btn;
    private Button btn_create;
    private String b_name = "", b_address = "", b_city = "", zip_code = "", b_phone = "",
            b_web = "", b_desc = "", c_password = "", b_hours = "", language = "", b_catagory = "", b_state = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_business_profile);
        InitViews();
        AdaptersData();
        ClickViews();


    }

    private void InitViews() {
        business_name = findViewById(R.id.business_name);
        business_address = findViewById(R.id.business_address);
        city = findViewById(R.id.city);
        zip_code_optional = findViewById(R.id.zip_code_optional);
        business_phone = findViewById(R.id.business_phone);
        business_website = findViewById(R.id.business_website);
        business_desc = findViewById(R.id.business_desc);
        confirm_password = findViewById(R.id.confirm_password);

        sp_state = findViewById(R.id.sp_state);
        sp_business_hours = findViewById(R.id.sp_business_hours);
        sp_language_spoken = findViewById(R.id.sp_language_spoken);
        sp_business_cat = findViewById(R.id.sp_business_cat);

        profile_img = findViewById(R.id.profile_img);

        back_btn = findViewById(R.id.back_btn);
        btn_create = findViewById(R.id.btn_create);
    }

    private void ClickViews() {
        b_name = business_name.getText().toString().trim();
        b_address = business_address.getText().toString().trim();
        b_city = city.getText().toString().trim();
        zip_code = zip_code_optional.getText().toString().trim();
        b_phone = business_phone.getText().toString().trim();
        b_web = business_website.getText().toString().trim();
        b_desc = business_desc.getText().toString().trim();
        c_password = confirm_password.getText().toString().trim();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!b_name.isEmpty()) {
                    if (!b_city.isEmpty()) {
                        if (!b_phone.isEmpty()) {
                            if (!b_web.isEmpty()) {
                                if (!language.equals("Language Spoken")) {
                                    if (!b_desc.isEmpty()) {
                                        if (!c_password.isEmpty()) {

                                            // Hit Api Here
                                            //TODO: Work here dude ;)

                                            Toast.makeText(CreateBusinessProfileActivity.this, "Validated", Toast.LENGTH_SHORT).show();
                                        } else {
                                            confirm_password.setError("Field Required");
                                        }
                                    } else {
                                        business_desc.setError("Field Required");
                                    }
                                } else {
                                    Toast.makeText(CreateBusinessProfileActivity.this, "Select Language Spoken", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                business_website.setError("Field Required");
                            }
                        } else {
                            business_phone.setError("Field Required");
                        }
                    } else {
                        city.setError("Field Required");
                    }
                } else {
                    business_name.setError("Field Required");
                }
            }
        });

        profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    private void AdaptersData() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.business_hours_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_business_hours.setAdapter(adapter);
        sp_business_hours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                b_hours = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"b_hours",b_hours);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_lang = ArrayAdapter.createFromResource(this,
                R.array.language_array, android.R.layout.simple_spinner_item);
        adapter_lang.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_language_spoken.setAdapter(adapter_lang);
        sp_language_spoken.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                language = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"language",language);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_state = ArrayAdapter.createFromResource(this,
                R.array.state_array, android.R.layout.simple_spinner_item);
        adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_state.setAdapter(adapter_state);
        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                b_state = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"b_state",b_state);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayList<String> business_cat_list = new ArrayList<>();
        business_cat_list.add("B Cat 1");
        business_cat_list.add("B Cat 2");
        business_cat_list.add("B Cat 3");
        business_cat_list.add("B Cat 4");
        business_cat_list.add("B Cat 5");
        business_cat_list.add("B Cat 6");

        final List<KeyPairBoolData> sportsArray0 = new ArrayList<>();
        for (int i = 0; i < business_cat_list.size(); i++) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setId(i + 1);
            h.setName(business_cat_list.get(i));
            h.setSelected(false);
            sportsArray0.add(h);
        }
        sp_business_cat.setItems(sportsArray0, -1, new SpinnerListener() {

            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
                ArrayList<String> getSports = new ArrayList<>();
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).isSelected()) {
                        Toast.makeText(CreateBusinessProfileActivity.this, items.get(i).getName() + " : " + items.get(i).isSelected(), Toast.LENGTH_SHORT).show();
                        getSports.add(items.get(i).getName());
                    }
                }
            }
        });
    }
}
